﻿using System;
using caalhp.IcePluginAdapters;

namespace LookupService
{
    public class Program
    {
        private static ServiceAdapter _adapter;
        private static LookupServiceImplementation _implementation;

        static void Main(string[] args)
        {


            const string endpoint = "localhost";
            try
            {
                _implementation = new LookupServiceImplementation();
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Ice.Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();

        }

    }
}
