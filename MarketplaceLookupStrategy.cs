﻿using System;
using System.Collections.Generic;
using System.Net;
using MarketplaceContract;
using Newtonsoft.Json;
using System.Configuration;
using caalhp.Core.Utils.Helpers;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Diagnostics;


namespace LookupService
{



    public class MarketplaceLookupStrategy : ILookupStrategy
    {

        private readonly WebClient _webClient;
        private readonly Uri _baseAddress;

        public MarketplaceLookupStrategy()
        {
            // web client
            _webClient = new CookieWebClient();

            //_baseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings.Get("MarketAddress"));
            _baseAddress = new Uri("https://carestoremarket.azurewebsites.net");
            // Login into the website (at this point the Cookie Container will do the rest of the job for us, and save the cookie for the next calls)


            AuthorizeCaalhp();
        }

        private void AuthorizeCaalhp()
        {
            //System.Diagnostics.Debugger.Launch();
            var relativeUri = "api/caalhp/authorize/" + CaalhpConfigHelper.GetCaalhpId();
            var authUri = new Uri(_baseAddress, relativeUri);
            var nameValues = new NameValueCollection();
            _webClient.UploadValues(authUri, "POST", nameValues);
        }


        public string Lookup(string value)
        {
            var res = GetDriverInfo(value);
            return res != null ? res.Url : String.Empty;
        }

        private  Driver GetDriverInfo(string id)
        {
            //TODO: get this url from config 
            //const string url = "http://carestoremarket.azurewebsites.net/api/drivers/nfc/";
            try
            {
                id = id.Remove(id.IndexOf("\0", 0));
                //var webClient = new WebClient();

                var address = new Uri(_baseAddress, "api/drivers/nfc/");
                _webClient.Headers["Content-type"] = ContentType.Json;
                var request = address + id;
                var content = _webClient.DownloadString(request);
                var result = JsonConvert.DeserializeObject<Driver>(content);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


    }
    public class CookieWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; private set; }

        /// <summary>
        /// This will instanciate an internal CookieContainer.
        /// </summary>
        public CookieWebClient()
        {
            CookieContainer = new CookieContainer();
        }

        /// <summary>
        /// Use this if you want to control the CookieContainer outside this class.
        /// </summary>
        public CookieWebClient(CookieContainer cookieContainer)
        {
            CookieContainer = cookieContainer;
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address) as HttpWebRequest;
            if (request == null) return base.GetWebRequest(address);
            request.CookieContainer = CookieContainer;
            return request;
        }
    }

    public class ContentType
    {
        public const string Json = "application/json";
    }
}