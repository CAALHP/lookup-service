﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using caalhp.Core.Utils.Helpers.Serialization.JSON;

namespace LookupService
{
    public class LookupServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly ILookupStrategy _lookup;

        public LookupServiceImplementation()
        {
            _lookup = new MarketplaceLookupStrategy();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(RFIDFoundEvent e)
        {
            Task.Run(() =>
            {
                var url = _lookup.Lookup(e.Tag);
                if (string.IsNullOrWhiteSpace(url)) return;
                var command = new DownloadDeviceDriverEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    DeviceDriverFileName = url
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _host.Host.ReportEvent(serializedCommand);
            }).Wait();
        }

        public string GetName()
        {
            return "LookupService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(RFIDFoundEvent)), _processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}