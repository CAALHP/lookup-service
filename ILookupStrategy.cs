﻿namespace LookupService
{
    public interface ILookupStrategy
    {
        string Lookup(string value);
    }
}